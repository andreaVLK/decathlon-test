export interface Location {
  id: number | string;
  name: string;
  number?: string;
  address: string;
  city?: string;
  postalCode?: string;
  region?: string;
  phone?: string;
  latitude?: string;
  longitude?: string;
  stockDelivery?: number;
  stockDeliveryBack?: number;
  slug?: string;
  warehouse?: Location;
  isWarehouse?: boolean;
  active?: boolean;
}

export type Status = 'idle' | 'loading' | 'loaded';

export type LocationList = Location[];

export type StateError = {
  message: string;
  code: number;
};

export type LocationState = {
  query: string;
  status: Status;
  data: LocationList;
  error?: StateError | null;
};