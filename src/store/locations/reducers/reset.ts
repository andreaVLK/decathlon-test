import { LocationState } from '../types/general';

export const resetLocationsCase = (state: LocationState): LocationState => ({
  ...state,
  status: 'idle',
  data: [],
  error: null,
});