/**
 * @packageDocumentation
 * @module components/molecules
 */

import React from 'react';
import { CardContainer } from '../../atoms/CardContainer';
import { Flex } from '../../atoms/Flex';
import { Icon } from '../../atoms/Icon';
import { Typo } from '../../atoms/Typo';
import { IconName } from '../../atoms/Icon/IconMap';
import { Spacer } from '../../atoms/Spacer';
import { memoize } from '../../../helpers/typedMemo';

type Props = {
  icon: IconName;
  title: string;
  text: string;
};

export const HilightCard: React.FC<Props> = memoize(({
  icon,
  title,
  text,
}) => {
  return (
    <CardContainer height="250px">
      <Flex.Box flexDirection="column" justifyContent="center" alignItems="center" height="100%">
        <Icon iconName={icon} width="60px" />
        <Spacer height="15px" />
        <Typo type="h3">{title?.toUpperCase()}</Typo>
        <Spacer height="30px" />
        <Typo type="p">{text}</Typo>
      </Flex.Box>
    </CardContainer>
  );
});

HilightCard.displayName = 'HilightCard';