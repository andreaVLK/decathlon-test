declare namespace NodeJS {
  export interface ProcessEnv {
    REACT_APP_X_API_KEY: string;
    REACT_APP_GOOGLE_API_KEY: string;
    REACT_APP_NAME: string;
    REACT_APP_LOCATION_API_BASE_URL: string;
    REACT_APP_GOOGLE_PLACES_BASE_URL: string;
    REACT_APP_USE_LOCATION_MOCK: string;
  }
}
