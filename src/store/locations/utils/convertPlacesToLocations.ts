import { v4 as createUuid } from 'uuid';
import { PlacesApiResponse } from '../types';
import { LocationList, Location } from '../types/general';

export const convertPlacesToLocations = (
  locations: PlacesApiResponse,
): LocationList => {
  const placeList = locations.predictions;

  return placeList.map(
    (place): Location => ({
      id: createUuid(),
      name: place.structured_formatting.main_text,
      address: place.description,
    }),
  );
};
