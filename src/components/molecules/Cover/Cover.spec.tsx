import React from 'react';
import { ThemeProvider } from 'styled-components';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { configure, mount } from 'enzyme';
import { Cover } from './Cover';
import sampleImage from '../../../assets/images/photos/snowboard.jpg';
import { theme } from '../../../styles';


configure({ adapter: new Adapter() });

describe('when render', () => {
  describe('and has all props with a value', () => {
    const container = mount(
      <ThemeProvider theme={theme}>
        <Cover image={sampleImage} bigMessage="HEY" smallMessage="a test here!" />
      </ThemeProvider>,
    );
    it('should match snapshot', () => {
      expect(container.html()).toMatchSnapshot();
    });

    it('should have a h1 "HEY" text', () => {
      expect(container.find('h1').text()).toEqual('HEY');
    });

    it('should have a h2 "a test here!" text', () => {
      expect(container.find('h2').text()).toEqual('a test here!');
    });

    it('should have an image with the correct src', () => {
      expect(container.find('img').props().src).toEqual(sampleImage);
    });
  });
});
