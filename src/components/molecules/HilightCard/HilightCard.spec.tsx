import React from 'react';
import { ThemeProvider } from 'styled-components';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { configure, mount } from 'enzyme';
import { HilightCard } from './HilightCard';
import sampleIcon from '../../../assets/images/sport-icons/snowboard.svg';
import { theme } from '../../../styles/theme';

configure({ adapter: new Adapter() });

describe('when render', () => {
  describe('and has all props with a value', () => {
    const sampleBigMessage = 'a title here';
    const sampleSmallMessage = 'a text here';
    const container = mount(
      <ThemeProvider theme={theme}>
        <HilightCard
          icon="snowboard"
          title={sampleBigMessage}
          text={sampleSmallMessage}
        />
      </ThemeProvider>,
    );

    container.update();
    it('should match snapshot', () => {
      expect(container.html()).toMatchSnapshot();
    });

    it('should render the correct icon', () => {
      expect(container.find('img').at(0).props().src).toBeDefined();
      expect(container.find('img').at(0).props().src).toEqual(sampleIcon);
    });

    it('should render the correct text', () => {
      expect(container.find('h3').text()).toBeDefined();
      expect(container.find('h3').text()).toEqual(
        sampleBigMessage.toUpperCase(),
      );
    });

    it('should render the correct text', () => {
      expect(container.find('p').text()).toBeDefined();
      expect(container.find('p').text()).toEqual(sampleSmallMessage);
    });
  });
});
