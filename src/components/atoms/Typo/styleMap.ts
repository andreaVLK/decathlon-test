import { ThemeSize, ThemeColor, ThemeFontWeight, ThemeFontStyle } from '../../../styles/types';


export type Type = 'h1' | 'h2' | 'h3' | 'p' | 'ps' | 'pxs' | 'a';

export type DefaultStyle = {
  tag: 'h1' | 'h2' | 'h3' | 'p';
  size: ThemeSize;
  color: ThemeColor;
  weight: ThemeFontWeight;
  style: ThemeFontStyle;
  shadow?: string;
  cursor?: 'default' | 'pointer';
  withHoverFx?: true;
};

export type Map = Record<Type, DefaultStyle>;

export const map: Map = {
  h1: {
    tag: 'h1',
    size: 'xxl',
    color: 'white',
    weight: 'bold',
    style: 'italic',
    shadow: 'large',
  },
  h2: {
    tag: 'h2',
    size: 'xl',
    color: 'white',
    weight: 'bold',
    style: 'default',
    shadow: 'small',
  },
  h3: {
    tag: 'h3',
    size: 'l',
    color: 'black',
    weight: 'bold',
    style: 'default',
  },
  p: {
    tag: 'p',
    size: 'm',
    color: 'black',
    weight: 'regular',
    style: 'default',
  },
  ps: {
    tag: 'p',
    size: 's',
    color: 'black',
    weight: 'regular',
    style: 'default',
  },
  pxs: {
    tag: 'p',
    size: 'xs',
    color: 'black',
    weight: 'regular',
    style: 'default',
  },
  a: {
    tag: 'p',
    size: 'm',
    color: 'black',
    weight: 'regular',
    style: 'default',
    cursor: 'pointer',
    withHoverFx: true,
  },
};