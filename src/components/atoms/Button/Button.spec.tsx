import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Button } from './Button';

describe('when render', () => {
  it('should work with no props', () => {
    const tree = renderer.create(<Button />).toJSON();
    expect(tree).toHaveStyleRule('opacity', '0.8', {
      modifier: ':hover',
    });
    expect(tree).toMatchSnapshot();
  });
  
  it('should work with width props', () => {
    const tree = renderer.create(<Button width="100px" />).toJSON();
    expect(tree).toHaveStyleRule('opacity', '0.8', {
      modifier: ':hover',
    });
    expect(tree).toHaveStyleRule('width', '100px');
    expect(tree).toMatchSnapshot();
  });
});

