export const locationMock = [
  {
    id: 16,
    name: 'Segrate, Milano',
    number: '315',
    address: 'via Rinaldo Piaggio 5',
    city: 'Segrate',
    postalCode: '20090',
    region: 'Lombardia',
    phone: '0249537459',
    latitude: '45.485522',
    longitude: '9.263937',
    stockDelivery: 2,
    stockDeliveryBack: 3,
    slug: 'segrate-milano-segrate-undefined',
    warehouse: {
      id: 40,
      name: 'LABORATORIO REGIONALE BASIANO',
      number: '438',
      address: 'Viale delle Industrie snc',
      city: 'BASIANO',
      postalCode: '20060',
      region: 'Emilia-Romagna',
      phone: null,
      latitude: '45.581768',
      longitude: '9.464901',
      stockDelivery: 0,
      stockDeliveryBack: 0,
      slug: 'laboratorio-regionale-basiano-basiano-mi',
      warehouse: null,
      isWarehouse: true,
      active: true,
    },
    isWarehouse: false,
    active: true,
  },
  {
    id: 1,
    name: 'Baranzate, MI',
    number: '091',
    address: 'Via Milano SS Varesina',
    city: 'Baranzate',
    postalCode: '20021',
    region: 'Lombardia',
    phone: '0238309510',
    latitude: '45.531735',
    longitude: '9.105119',
    stockDelivery: 3,
    stockDeliveryBack: 4,
    slug: 'baranzatemi_baranzate_undefined',
    warehouse: {
      id: 40,
      name: 'LABORATORIO REGIONALE BASIANO',
      number: '438',
      address: 'Viale delle Industrie snc',
      city: 'BASIANO',
      postalCode: '20060',
      region: 'Emilia-Romagna',
      phone: null,
      latitude: '45.581768',
      longitude: '9.464901',
      stockDelivery: 0,
      stockDeliveryBack: 0,
      slug: 'laboratorio-regionale-basiano-basiano-mi',
      warehouse: null,
      isWarehouse: true,
      active: true,
    },
    isWarehouse: false,
    active: true,
  },
  {
    id: 25,
    name: 'Milano Cairoli, Milano',
    number: '342',
    address: 'Foro Bonaparte 74/76',
    city: 'Milano',
    postalCode: '20121',
    region: 'Lombardia',
    phone: '0280509755',
    latitude: '45.469000',
    longitude: '9.182300',
    stockDelivery: 3,
    stockDeliveryBack: 4,
    slug: 'milano-cairoli-milano-milano-undefined',
    warehouse: {
      id: 40,
      name: 'LABORATORIO REGIONALE BASIANO',
      number: '438',
      address: 'Viale delle Industrie snc',
      city: 'BASIANO',
      postalCode: '20060',
      region: 'Emilia-Romagna',
      phone: null,
      latitude: '45.581768',
      longitude: '9.464901',
      stockDelivery: 0,
      stockDeliveryBack: 0,
      slug: 'laboratorio-regionale-basiano-basiano-mi',
      warehouse: null,
      isWarehouse: true,
      active: true,
    },
    isWarehouse: false,
    active: true,
  },
  {
    id: 2,
    name: 'Corsico, Milano',
    number: '120',
    address: "Viale dell'Industria 2/4",
    city: 'Corsico',
    postalCode: '20094',
    region: 'Lombardia',
    phone: '024580490',
    latitude: '45.432100',
    longitude: '9.081800',
    stockDelivery: 3,
    stockDeliveryBack: 3,
    slug: 'corsico-milano-corsico-undefined',
    warehouse: {
      id: 40,
      name: 'LABORATORIO REGIONALE BASIANO',
      number: '438',
      address: 'Viale delle Industrie snc',
      city: 'BASIANO',
      postalCode: '20060',
      region: 'Emilia-Romagna',
      phone: null,
      latitude: '45.581768',
      longitude: '9.464901',
      stockDelivery: 0,
      stockDeliveryBack: 0,
      slug: 'laboratorio-regionale-basiano-basiano-mi',
      warehouse: null,
      isWarehouse: true,
      active: true,
    },
    isWarehouse: false,
    active: true,
  },
  {
    id: 17,
    name: 'Cinisello, Milano',
    number: '316',
    address: 'Viale Brianza, 2   ',
    city: 'Cinisello Balsamo',
    postalCode: '20092',
    region: 'Lombardia',
    phone: '0261294473 ',
    latitude: '45.549700',
    longitude: '9.238200',
    stockDelivery: 3,
    stockDeliveryBack: 4,
    slug: 'cinisello-milano-cinisello-balsamo-mi',
    warehouse: {
      id: 40,
      name: 'LABORATORIO REGIONALE BASIANO',
      number: '438',
      address: 'Viale delle Industrie snc',
      city: 'BASIANO',
      postalCode: '20060',
      region: 'Emilia-Romagna',
      phone: null,
      latitude: '45.581768',
      longitude: '9.464901',
      stockDelivery: 0,
      stockDeliveryBack: 0,
      slug: 'laboratorio-regionale-basiano-basiano-mi',
      warehouse: null,
      isWarehouse: true,
      active: true,
    },
    isWarehouse: false,
    active: true,
  },
  {
    id: 21,
    name: 'Carugate, Milano',
    number: '331',
    address: 'Via Guido Rossa',
    city: 'Carugate',
    postalCode: '20061',
    region: 'Lombardia',
    phone: '0292151970',
    latitude: '45.547600',
    longitude: '9.334900',
    stockDelivery: 3,
    stockDeliveryBack: 4,
    slug: 'carugate-milano-carugate-undefined',
    warehouse: {
      id: 40,
      name: 'LABORATORIO REGIONALE BASIANO',
      number: '438',
      address: 'Viale delle Industrie snc',
      city: 'BASIANO',
      postalCode: '20060',
      region: 'Emilia-Romagna',
      phone: null,
      latitude: '45.581768',
      longitude: '9.464901',
      stockDelivery: 0,
      stockDeliveryBack: 0,
      slug: 'laboratorio-regionale-basiano-basiano-mi',
      warehouse: null,
      isWarehouse: true,
      active: true,
    },
    isWarehouse: false,
    active: true,
  },
  {
    id: 43,
    name: 'Rozzano, Milano',
    number: '467',
    address: 'Via Eugenio Curiel, 10/12 fronte cc Fiordaliso',
    city: 'Rozzano',
    postalCode: '20089',
    region: 'Lombardia',
    phone: '0257506818',
    latitude: '45.376000',
    longitude: '9.142800',
    stockDelivery: 3,
    stockDeliveryBack: 4,
    slug: 'rozzano-milano-rozzano-undefined',
    warehouse: {
      id: 40,
      name: 'LABORATORIO REGIONALE BASIANO',
      number: '438',
      address: 'Viale delle Industrie snc',
      city: 'BASIANO',
      postalCode: '20060',
      region: 'Emilia-Romagna',
      phone: null,
      latitude: '45.581768',
      longitude: '9.464901',
      stockDelivery: 0,
      stockDeliveryBack: 0,
      slug: 'laboratorio-regionale-basiano-basiano-mi',
      warehouse: null,
      isWarehouse: true,
      active: true,
    },
    isWarehouse: false,
    active: true,
  },
  {
    id: 108,
    name: 'Biella',
    number: '1164',
    address: 'VIA MILANO 50',
    city: 'BIELLA',
    postalCode: '13900',
    region: 'Piemonte',
    phone: '0152400004',
    latitude: '45.568710',
    longitude: '8.069757',
    stockDelivery: 3,
    stockDeliveryBack: 3,
    slug: 'biella-biella-undefined',
    warehouse: {
      id: 113,
      name: 'LABORATORIO REGIONAL BRANDIZZO',
      number: '1326',
      address: 'Via Torino 474',
      city: 'Brandizzo',
      postalCode: '10032',
      region: 'Emilia-Romagna',
      phone: null,
      latitude: '45.169354',
      longitude: '7.827871',
      stockDelivery: 0,
      stockDeliveryBack: 0,
      slug: 'laboratorio-regional-brandizzo-brandizzo-to',
      warehouse: null,
      isWarehouse: true,
      active: true,
    },
    isWarehouse: false,
    active: true,
  },
  {
    id: 105,
    name: 'Gallarate, Varese',
    number: '1160',
    address: 'Viale Milano 175',
    city: 'Gallarate',
    postalCode: '21013',
    region: 'Lombardia',
    phone: '0331701660',
    latitude: '45.642450',
    longitude: '8.819817',
    stockDelivery: 3,
    stockDeliveryBack: 3,
    slug: 'gallarate-varese-gallarate-undefined',
    warehouse: {
      id: 113,
      name: 'LABORATORIO REGIONAL BRANDIZZO',
      number: '1326',
      address: 'Via Torino 474',
      city: 'Brandizzo',
      postalCode: '10032',
      region: 'Emilia-Romagna',
      phone: null,
      latitude: '45.169354',
      longitude: '7.827871',
      stockDelivery: 0,
      stockDeliveryBack: 0,
      slug: 'laboratorio-regional-brandizzo-brandizzo-to',
      warehouse: null,
      isWarehouse: true,
      active: true,
    },
    isWarehouse: false,
    active: true,
  },
  {
    id: 122,
    name: 'Novara',
    number: '2025',
    address: 'Corso Milano 116',
    city: 'Novara',
    postalCode: '28100',
    region: 'Piemonte',
    phone: '03211589316',
    latitude: '45.443068',
    longitude: '8.655070',
    stockDelivery: 3,
    stockDeliveryBack: 3,
    slug: 'novara-novara-undefined',
    warehouse: {
      id: 113,
      name: 'LABORATORIO REGIONAL BRANDIZZO',
      number: '1326',
      address: 'Via Torino 474',
      city: 'Brandizzo',
      postalCode: '10032',
      region: 'Emilia-Romagna',
      phone: null,
      latitude: '45.169354',
      longitude: '7.827871',
      stockDelivery: 0,
      stockDeliveryBack: 0,
      slug: 'laboratorio-regional-brandizzo-brandizzo-to',
      warehouse: null,
      isWarehouse: true,
      active: true,
    },
    isWarehouse: false,
    active: true,
  },
  {
    id: 146,
    name: 'Portello, Milano',
    number: '2317',
    address: 'Via Grosotto, 7,  Milano MI',
    city: 'Milano',
    postalCode: '20149',
    region: 'Lombardia',
    phone: '0235981043',
    latitude: '45.490847',
    longitude: '9.146994',
    stockDelivery: 4,
    stockDeliveryBack: 4,
    slug: 'portello-milano-milano-',
    warehouse: {
      id: 40,
      name: 'LABORATORIO REGIONALE BASIANO',
      number: '438',
      address: 'Viale delle Industrie snc',
      city: 'BASIANO',
      postalCode: '20060',
      region: 'Emilia-Romagna',
      phone: null,
      latitude: '45.581768',
      longitude: '9.464901',
      stockDelivery: 0,
      stockDeliveryBack: 0,
      slug: 'laboratorio-regionale-basiano-basiano-mi',
      warehouse: null,
      isWarehouse: true,
      active: true,
    },
    isWarehouse: false,
    active: true,
  },
  {
    id: 10,
    name: 'Rescaldina, Milano',
    number: '274',
    address: 'Via Togliatti 2 SS Saronnese',
    city: 'Rescaldina',
    postalCode: '20027',
    region: 'Lombardia',
    phone: '0331 578895',
    latitude: '45.618900',
    longitude: '8.955100',
    stockDelivery: 3,
    stockDeliveryBack: 3,
    slug: 'rescaldina-milano-rescaldina-undefined',
    warehouse: {
      id: 113,
      name: 'LABORATORIO REGIONAL BRANDIZZO',
      number: '1326',
      address: 'Via Torino 474',
      city: 'Brandizzo',
      postalCode: '10032',
      region: 'Emilia-Romagna',
      phone: null,
      latitude: '45.169354',
      longitude: '7.827871',
      stockDelivery: 0,
      stockDeliveryBack: 0,
      slug: 'laboratorio-regional-brandizzo-brandizzo-to',
      warehouse: null,
      isWarehouse: true,
      active: true,
    },
    isWarehouse: false,
    active: true,
  },
];
