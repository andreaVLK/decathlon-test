import { MenuItem } from './types';

export const menuMock: MenuItem[] = [
  {
    alias: 'choose',
    label: 'Scegli',
    url: '#',
    icon: 'shop',
  },
  {
    alias: 'faq',
    label: 'Domande frequenti',
    url: '#',
    icon: 'faq',
  },
  {
    alias: 'contacts',
    label: 'Contatti',
    url: '#',
    icon: 'mail',
  },
  {
    alias: 'cart',
    label: 'Carrello',
    url: '#',
    icon: 'cart',
  },
  {
    alias: 'login',
    label: 'Login',
    url: '#',
    icon: 'user',
  },
  {
    alias: 'langauge',
    label: 'italiano',
    url: '#',
    icon: 'it',
  },
];