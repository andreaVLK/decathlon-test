/**
 * @packageDocumentation
 * @module components/atoms
 */

import styled from 'styled-components';

type Props = {
  width?: string;
  height?: string;
};

export const Spacer = styled.div<Props>`
  width: ${({ width = '0' }: Props) => width};
  height: ${({ height = '0' }: Props) => height};
`;

Spacer.displayName = 'Spacer';
