/**
 * @packageDocumentation
 * @module components/molecules
 */

import React, { useMemo } from 'react';
import { memoize } from '../../../helpers';
import { Flex } from '../../atoms/Flex';
import { Icon } from '../../atoms/Icon';
import { Spacer } from '../../atoms/Spacer';
import { Typo } from '../../atoms/Typo/Typo';
import { MenuItem } from './types';

type Props = {
  items: MenuItem[];
};

type ItemProps = {
  item: MenuItem;
  isLast: boolean;
};

const ListItem: React.FC<ItemProps> = memoize(({ item, isLast }) => {
  return (
    <Flex.Col
      paddingLeft="xs"
      paddingRight={isLast ? 'xs' : 's'}
      width="auto"
    >
      <Flex.Box alignItems="center">
        <Icon iconName={item.icon} width="20px" height="20px" />
        <Spacer width="8px" />
        <Typo type="a">{item.label}</Typo>
      </Flex.Box>
    </Flex.Col>
  );
});

export const InlineMenu: React.FC<Props> = ({ items }) => {
  const renderList = useMemo(() => {
    return items.map((item: MenuItem, index: number) => {
      const isLast = index === items.length - 1;
      return <ListItem key={item.alias} item={item} isLast={isLast} />;
    });
  }, [items]);
  return (
    <Flex.Box
      paddingBottom="s"
      paddingLeft="s"
      paddingRight="s"
      paddingTop="s"
      width="auto"
      alignItems="center"
    >
      {renderList}
    </Flex.Box>
  );
};

InlineMenu.displayName = 'InlineMenu';