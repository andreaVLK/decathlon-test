/**
 * @packageDocumentation
 * @module components/organisms
 */

import React, { useCallback } from 'react';
import Autocomplete from 'react-autocomplete';
import { Flex } from '../../../atoms/Flex';
import { Typo } from '../../../atoms/Typo';
import { Input } from '../../../atoms/Input';
import { useLocation } from '../../../../store/locations';
import { Location } from '../../../../store/locations/types';
import { memoize } from '../../../../helpers';

export const AutocompleteInput: React.FC = memoize(() => {
  const { setSearchQuery, locationList, locationQuery } = useLocation();

  const onAutocompleteChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setSearchQuery(e.currentTarget.value);
    },
    [setSearchQuery],
  );

  const onAutocompleteSelect = useCallback(
    (_: string, item: Location) => {
      setSearchQuery(`${item.name} - ${item.address}`);
    },
    [setSearchQuery],
  );

  const renderInput = useCallback(
    (props: React.HTMLAttributes<HTMLInputElement>) => {
      return <Input placeholder="CERCA" {...props} />;
    },
    [],
  );

  const getItemValue = useCallback((item: Location) => item.id.toString(), []);

  const renderItem = useCallback((item: Location, isHilighted: boolean) => {
    return (
      <Flex.Box
        key={item.id}
        paddingBottom="m"
        paddingLeft="m"
        paddingRight="m"
        paddingTop="m"
        background={isHilighted ? 'light' : 'white'}
      >
        <Typo type="p">
          {item.name.toUpperCase()} - {item.address}
        </Typo>
      </Flex.Box>
    );
  }, []);

  return (
    <Autocomplete
      menuStyle={{
        borderRadius: '3px',
        boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
        background: 'rgba(255, 255, 255, 0.9)',
        padding: '2px 0',
        fontSize: '90%',
        position: 'fixed',
        overflow: 'auto',
        maxHeight: '50%',
        zIndex: 99,
      }}
      wrapperStyle={{ width: '30%' }}
      items={locationList}
      value={locationQuery}
      getItemValue={getItemValue}
      onChange={onAutocompleteChange}
      onSelect={onAutocompleteSelect}
      renderInput={renderInput}
      renderItem={renderItem}
    />
  );
});


AutocompleteInput.displayName = 'AutocompleteInput';