import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Icon } from './Icon';

describe('when render', () => {
  it('should work with width prop and iconName', () => {
    const tree = renderer.create(<Icon width="100px" iconName="it" />).toJSON();
    expect(tree).toHaveStyleRule('width', '100px');
    expect(tree).toMatchSnapshot();
  });

  it('should work with width prop and iconName', () => {
    const tree = renderer.create(<Icon width="100px" height="100px" iconName="it" />).toJSON();
    expect(tree).toHaveStyleRule('height', '100px');
    expect(tree).toMatchSnapshot();
  });
});
