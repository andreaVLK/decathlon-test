export type ThemeSize = 'xxs' | 'xs' | 's' | 'm' | 'l' | 'xl' | 'xxl';
export type ThemeFontFamily = 'serif';
export type ThemeFontStyle = 'default' | 'italic';
export type ThemeFontWeight = 'regular' | 'bold';
export type ThemeColor = 'primary' | 'secondary' | 'accent' | 'dark' | 'light' | 'white' | 'black';

export type ThemeSpaces = Record<ThemeSize, string>;
export type ThemeFontSizes =  Record<ThemeSize, string>;
export type ThemeFontFamilies =  Record<ThemeFontFamily, string>;
export type ThemeFontWeights =  Record<ThemeFontWeight, string>;
export type ThemeFontStyles =  Record<ThemeFontStyle, string>;
export type ThemeColors = Record<ThemeColor, string>;

export type Theme = {
  colors: ThemeColors;
  spaces: ThemeSpaces;
  fontFamilies: ThemeFontFamilies;
  fontSizes: ThemeFontSizes;
  fontWeights: ThemeFontWeights;
  fontStyles: ThemeFontStyles;
};