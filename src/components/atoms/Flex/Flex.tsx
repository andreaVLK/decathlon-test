/**
 * @packageDocumentation
 * @module components/atoms
 */

import styled, { css } from 'styled-components';
import { memoize } from '../../../helpers';
import {  Theme, ThemeSize } from '../../../styles';
import { ThemeColor } from '../../../styles/types';


type BoxProps = {
  width?: string;
  height?: string;
  alignItems?: 'center' | 'flex-start' | 'flex-end';
  justifyContent?:
  | 'flex-start'
  | 'flex-end'
  | 'center'
  | 'space-between'
  | 'space-around'; 
  flexDirection?: 'row' | 'column';
  paddingLeft?: ThemeSize;
  paddingRight?: ThemeSize;
  paddingTop?: ThemeSize;
  paddingBottom?: ThemeSize;
  background?: ThemeColor;
  theme: Theme,
  withShadow?: boolean;
};

const Box = memoize(styled.div<BoxProps>`
  display: flex;
  position: relative;
  width: ${({ width = '100%' }: BoxProps) => width };
  height: ${({ height = 'auto' }: BoxProps) => height };
  align-items: ${({ alignItems = 'flex-start' }: BoxProps) => alignItems };
  justify-content: ${({ justifyContent = 'flex-start' }: BoxProps) => justifyContent };
  flex-direction: ${({ flexDirection = 'row' }: BoxProps) => flexDirection };
  padding-left: ${({ paddingLeft, theme }: BoxProps) => paddingLeft ? theme?.spaces[paddingLeft] : 0 };
  padding-right: ${({ paddingRight, theme }: BoxProps) => paddingRight ? theme?.spaces[paddingRight] : 0 };
  padding-top: ${({ paddingTop, theme }: BoxProps) => paddingTop ? theme?.spaces[paddingTop] : 0 };
  padding-bottom: ${({ paddingBottom, theme }: BoxProps) => paddingBottom ? theme?.spaces[paddingBottom] : 0 };
  background: ${({ background, theme }: BoxProps) => background ? theme?.colors[background] : 'transparent' };
  ${({ withShadow }: BoxProps) => withShadow && css`
    box-shadow: -2px 5px 20px rgba(0,0,0,0.2)
  `};
  box-sizing: border-box;
`);

Box.displayName = 'Box';

type ColProps = {
  width?: string;
  height?: string;
  alignSelf?: 'center' | 'flex-start' | 'flex-end' | 'auto';
  justifySelf?:
  | 'auto'
  | 'flex-start'
  | 'flex-end'
  | 'center'
  | 'space-between'
  | 'space-arount';
  paddingHorizontal?: ThemeSize;
  paddingLeft?: ThemeSize;
  paddingRight?: ThemeSize;
  paddingTop?: ThemeSize;
  paddingBottom?: ThemeSize;
  background?: ThemeColor;
};

const Col = memoize(styled.div<ColProps>`
  display: flex;
  position: relative;
  width: ${({ width = '100%' }: ColProps) => width };
  height: ${({ height = 'auto' }: ColProps) => height };
  align-self: ${({ alignSelf = 'auto' }: ColProps) => alignSelf };
  justify-self: ${({ justifySelf = 'auto' }: ColProps) => justifySelf };
  padding-left: ${({ paddingLeft, theme }: BoxProps) => paddingLeft ? theme?.spaces[paddingLeft] || '10px' : 0 };
  padding-right: ${({ paddingRight, theme }: BoxProps) => paddingRight ? theme?.spaces[paddingRight] : 0 };
  padding-top: ${({ paddingTop, theme }: BoxProps) => paddingTop ? theme?.spaces[paddingTop] : 0 };
  padding-bottom: ${({ paddingBottom, theme }: BoxProps) => paddingBottom ? theme?.spaces[paddingBottom] : 0 };
  background: ${({ background, theme }: BoxProps) => background ? theme?.colors[background] : 'transparent' };
  box-sizing: border-box;
  flex-direction: ${({ flexDirection = 'row' }: BoxProps) => flexDirection };
`);

Col.displayName = 'Col';

const CompleteComponent = {};

export const Flex = Object.assign(CompleteComponent, {
  Box,
  Col,
});
