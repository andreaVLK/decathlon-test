import { createSelector } from '@reduxjs/toolkit';
import { LocationState, Status, LocationList } from '../types/general';

type MaimStateSlice = {
  location: LocationState;
};
const getLocationState = (state: MaimStateSlice) => state.location;

export const getLocationStatus = createSelector(
  getLocationState,
  (locationState): Status => locationState.status,
);


export const getLocationList = createSelector(
  getLocationState,
  (locationState): LocationList => locationState.data,
);

export const getLocationError = createSelector(
  getLocationState,
  (locationState): LocationList => locationState.data,
);

export const getLocationQuery = createSelector(
  getLocationState,
  (locationState): string => locationState.query,
);


