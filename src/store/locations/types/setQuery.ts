import { PayloadAction } from '@reduxjs/toolkit';

export type SetLocationQueryPayload = { query: string };
export type SetLocationQueryAction = PayloadAction<SetLocationQueryPayload>;
