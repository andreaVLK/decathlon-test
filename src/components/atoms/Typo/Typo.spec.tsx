import React from 'react';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { shallow, configure } from 'enzyme';
import { Typo } from './Typo';

configure({ adapter: new Adapter() });

describe('when render', () => {
  describe('and has type h1', () => {
    const container = shallow(<Typo type="h1" />);
    it('should match snapshot', () => {
      expect(container.html()).toMatchSnapshot();
    });
    it('should render an h1 text elemnt', () => {
      expect(container.find('h1'));
    });

    it('should render with default styles', () => {
      const { defaultStyle } = container.get(0).props;
      expect(defaultStyle).toEqual({
        tag: 'h1',
        size: 'xxl',
        color: 'white',
        weight: 'bold',
        style: 'italic',
        shadow: 'large',
      });
    });
  });

  describe('and has type h2', () => {
    const container = shallow(<Typo type="h2" />);
    it('should match snapshot', () => {
      expect(container.html()).toMatchSnapshot();
    });
    it('should render an h2 text elemnt', () => {
      expect(container.find('h2'));
    });

    it('should render with default styles', () => {
      const { defaultStyle } = container.get(0).props;
      expect(defaultStyle).toEqual({
        tag: 'h2',
        size: 'xl',
        color: 'white',
        weight: 'bold',
        style: 'default',
        shadow: 'small',
      });
    });
  });

  describe('and has type h3', () => {
    const container = shallow(
      <Typo type="h3" />,
    );
    it('should match snapshot', () => {
      expect(container.html()).toMatchSnapshot();
    });
    it('should render an h3 text elemnt', () => {
      expect(container.find('h3'));
    });

    it('should render with default styles', () => {
      const { defaultStyle } = container.get(0).props;
      expect(defaultStyle).toEqual({
        tag: 'h3',
        size: 'l',
        color: 'black',
        weight: 'bold',
        style: 'default',
      });
    });
  });

  describe('and has type p', () => {
    const container = shallow(<Typo type="p" />);
    it('should match snapshot', () => {
      expect(container.html()).toMatchSnapshot();
    });
    it('should render an p text elemnt', () => {
      expect(container.find('p'));
    });

    it('should render with default styles', () => {
      const { defaultStyle } = container.get(0).props;
      expect(defaultStyle).toEqual({
        tag: 'p',
        size: 'm',
        color: 'black',
        weight: 'regular',
        style: 'default',
      });
    });
  });

  describe('and has type a', () => {
    const container = shallow(<Typo type="a" />);
    it('should match snapshot', () => {
      expect(container.html()).toMatchSnapshot();
    });
    it('should render an a text elemnt', () => {
      expect(container.find('a'));
    });
  });

  describe('and has children', () => {
    const container = shallow(<Typo type="a">test</Typo>);
    it('should match snapshot', () => {
      expect(container.html()).toMatchSnapshot();
    });
    it('it should wrap a text', () => {
      expect(container.text()).toEqual('test');
    });
  });
});
