/**
 * @packageDocumentation
 * @module components/organisms
 */

import React from 'react';
import { memoize } from '../../../../helpers';
import { Flex } from '../../../atoms/Flex';

export const Wrapper: React.FC = memoize(({ children }) => (
  <Flex.Box
    background="white"
    withShadow
    paddingBottom="m"
    paddingLeft="m"
    paddingRight="m"
    paddingTop="m"
    justifyContent="space-between"
  >
    {children}
  </Flex.Box>
));

Wrapper.displayName = 'Wrapper';
