import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { CardContainer } from './CardContainer';

describe('when render', () => {
  it('should work with no props', () => {
    const tree = renderer.create(<CardContainer />).toJSON();
    expect(tree).toHaveStyleRule('width', 'auto');
    expect(tree).toHaveStyleRule('height', 'auto');
    expect(tree).toHaveStyleRule('padding', '20px');
    expect(tree).toMatchSnapshot();
  });

  it('should work with width prop', () => {
    const tree = renderer.create(<CardContainer width="100px" />).toJSON();
    expect(tree).toHaveStyleRule('width', '100px');
    expect(tree).toHaveStyleRule('height', 'auto');
    expect(tree).toHaveStyleRule('padding', '20px');
    expect(tree).toMatchSnapshot();
  });

  it('should work with height prop', () => {
    const tree = renderer.create(<CardContainer height="100px" />).toJSON();
    expect(tree).toHaveStyleRule('width', 'auto');
    expect(tree).toHaveStyleRule('height', '100px');
    expect(tree).toHaveStyleRule('padding', '20px');
    expect(tree).toMatchSnapshot();
  });
});
