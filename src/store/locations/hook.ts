import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getLocationList, getLocationStatus, getLocationError, getLocationQuery } from './selectors/general';
import { searchLocation as searchLocationAction, resetLocations, setLocationQuery } from './actions';

export const useLocation = () => {
  const dispatch = useDispatch();
  const locationList = useSelector(getLocationList);
  const locationStatus = useSelector(getLocationStatus);
  const locationError = useSelector(getLocationError);
  const locationQuery = useSelector(getLocationQuery);

  const searchLocation = useCallback(() => {
    if (locationQuery) {
      return dispatch(searchLocationAction());
    }
    return dispatch(resetLocations());
  }, [dispatch, locationQuery]);

  useEffect(() => {
    searchLocation();
  }, [searchLocation]);

  const setSearchQuery = useCallback((query: string) => {
    dispatch(setLocationQuery({ query }));
  }, [dispatch]);

  return {
    locationQuery,
    setSearchQuery,
    locationList,
    locationStatus,
    locationError,
  };
};