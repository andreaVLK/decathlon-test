import wretch from 'wretch';

export const locationApiBase = wretch(
  process.env.REACT_APP_LOCATION_API_BASE_URL,
).headers({
  'content-type': 'application/json',
  'x-api-key': process.env.REACT_APP_X_API_KEY || '',
});
