import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Input } from './Input';

describe('when render', () => {
  it('should work with no props', () => {
    const tree = renderer.create(<Input />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should work with value prop', () => {
    const tree = renderer.create(<Input name="login" value="test" />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
