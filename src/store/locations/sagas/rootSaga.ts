import { debounce } from 'redux-saga/effects';
import { searchLocationActionTypes } from '../actions';
import { searchLocationSaga } from './search';

export function* searchLocationRootSaga() {
  yield debounce(
    300,
    searchLocationActionTypes.trigger,
    searchLocationSaga,
  );
}
