/**
 * @packageDocumentation
 * @module components/atoms
 */

import styled from 'styled-components';
import { memoize } from '../../../helpers';
import { Theme } from '../../../styles/types';

type Props = {
  width?: string;
  height?: string;
  theme: Theme;
};

export const CardContainer = memoize(styled.div<Props>`
  box-sizing: border-box;
  width: ${({ width = 'auto' }: Props) => width};
  height: ${({ height = 'auto' }: Props) => height};
  border: ${({ theme }: Props) => theme?.colors?.light ? `2px solid ${theme?.colors?.light}` : '2px solid #eee'};
  padding: ${({ theme }: Props) => theme?.spaces?.l || '20px'};
`);

CardContainer.displayName = 'CardContainer';