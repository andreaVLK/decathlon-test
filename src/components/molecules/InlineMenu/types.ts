import { IconName } from '../../atoms/Icon/IconMap';

export type MenuSubItem = {
  alias: string;
  label: string;
  url: string;
};

export type MenuItem = {
  alias: string;
  url?: string;
  label: string;
  icon: IconName;
  notification?: {
    active: true;
    counter: number;
  };
  subItems?: MenuSubItem[];
};
