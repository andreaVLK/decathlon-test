/**
 * @packageDocumentation
 * @module components/atoms
 */


import styled from 'styled-components';
import { Theme } from '../../../styles';
import { memoize } from '../../../helpers';

type Props = {
  theme: Theme;
  width?: string;
};

export const Button = memoize(styled.button<Props>`
  cursor: pointer;
  border: none;
  background-color: ${({ theme }: Props) => theme?.colors?.primary || 'blue' };
  color: ${({ theme }: Props) => theme?.colors?.white || 'white' };
  padding: ${({ theme }: Props) => theme?.spaces?.s || '10px' };
  font-size: ${({ theme }: Props) => theme?.fontSizes?.m || '14px'};
  font-weight: ${({ theme }: Props) => theme?.fontWeights?.bold || 700 };
  font-family: ${({ theme }: Props) => theme?.fontFamilies?.serif || 'Roboto condensed' };
  width: ${({ width = 'auto' }: Props) => width };
  text-transform: uppercase;
  outline: none;
  &:hover {
    opacity: 0.8;
  }
`);

Button.displayName = 'Button';