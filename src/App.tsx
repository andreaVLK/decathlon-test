import React from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { setConfiguration } from 'react-grid-system';
import { store } from './configureStore';
import { AppProvided } from './AppProvided';
import { theme } from './styles/theme';
import { AppGlobalStyle } from './styles/AppGlobalStyle';
import './app.css';
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file

setConfiguration({ containerWidths: [540, 740, 960, 1140, 1380] });

export const App: React.FC = () => (
  <>
    <AppGlobalStyle />
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <AppProvided />
      </ThemeProvider>
    </Provider>
  </>
);
