/**
 * @packageDocumentation
 * @module components/organisms
 */

import React, {
  useCallback,
  useMemo,
  useRef,
  useState,
  useEffect,
} from 'react';
import { format } from 'date-fns';
import { DateRangePicker } from 'react-date-range';
import { memoize, IT } from '../../../../helpers';
import { Absolute } from '../../../atoms/Absolute';
import { Input } from '../../../atoms/Input';
import { Flex } from '../../../atoms/Flex/Flex';

export const CalendarInput: React.FC = memoize(() => {
  const calendarRef = useRef() as React.MutableRefObject<HTMLDivElement>;
  const inputRef = useRef() as React.MutableRefObject<HTMLInputElement>;
  const [showCalendar, setShowCalendar] = useState(false);
  const [dateRange, setDateRange] = useState({
    startDate: new Date(),
    endDate: new Date(),
    key: 'selection',
  });

  const onChange = useCallback(({ selection }) => {
    const { startDate, endDate } = selection;
    setDateRange((prevState) => ({
      ...prevState,
      startDate,
      endDate,
    }));
  }, []);

  const formattedDate = useMemo((): string => {
    const formattedStartDate = format(dateRange.startDate, 'dd/MM/yyyy');
    const formattedEndDate = format(dateRange.endDate, 'dd/MM/yyyy');
    return `${formattedStartDate} - ${formattedEndDate}`;
  }, [dateRange.startDate, dateRange.endDate]);

  const toggleCalendar = useCallback(() => {
    setShowCalendar((prevState) => !prevState);
  }, []);

  const closeCalendar = useCallback(() => {
    setShowCalendar(false);
  }, []);

  useEffect(() => {
    document.addEventListener('click', (e) => {
      if (
        // @ts-ignore
        e.target && !calendarRef?.current?.contains(e.target) &&
        e.target !== inputRef?.current
      ) {
        closeCalendar();
      }
    });

    return () => document.removeEventListener('click', () => {});
  }, [closeCalendar, showCalendar]);

  return (
    <Flex.Col width="25%">
      <Input
        ref={inputRef}
        onClick={toggleCalendar}
        placeholder="CERCA"
        style={{
          caretColor: 'transparent',
          cursor: 'pointer',
        }}
        onChange={() => {}}
        value={formattedDate}
        width="100%"
      />
      {showCalendar && (
        <Absolute top="100%" left="0" zIndex={99} ref={calendarRef}>
          <Flex.Box withShadow>
            <DateRangePicker
              locale={IT}
              onChange={onChange}
              ranges={[dateRange]}
              // @ts-ignore
              showDateDisplay={false}
            />
          </Flex.Box>
        </Absolute>
      )}
    </Flex.Col>
  );
});

CalendarInput.displayName = 'CalendarInput';
