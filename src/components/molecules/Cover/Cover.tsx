/**
 * @packageDocumentation
 * @module components/molecules
 */

import { Col, Container, Row } from 'react-grid-system';
import React, { ReactElement } from 'react';
import { Flex } from '../../atoms/Flex';
import { Image } from '../../atoms/Image';
import { Absolute } from '../../atoms/Absolute';
import { Typo } from '../../atoms/Typo';
import { Spacer } from '../../atoms/Spacer';
import { memoize } from '../../../helpers';

type Props = {
  image: string;
  bigMessage: string | ReactElement;
  smallMessage: string | ReactElement;
};

export const Cover: React.FC<Props> = memoize(
  ({ image, bigMessage, smallMessage }) => {
    return (
      <Flex.Box width="100%" height="600px" alignItems="center">
        <Absolute width="100%" height="100%" zIndex={-1}>
          <Image
            objectFit="cover"
            width="100%"
            height="100%"
            src={image}
            alt="snowboard"
          />
        </Absolute>
        <Container style={{ width: '100%' }}>
          <Row>
            <Flex.Box width="100%">
              <Col>
                <Typo type="h1">{bigMessage}</Typo>
                <Spacer height="50px" />
                <Typo type="h2">{smallMessage}</Typo>
              </Col>
            </Flex.Box>
          </Row>
        </Container>
      </Flex.Box>
    );
  },
);

Cover.displayName = 'Cover';
