/**
 * @packageDocumentation
 * @module components/organisms
 */

import React, { useCallback, useState } from 'react';
import Autocomplete from 'react-autocomplete';
import { Flex } from '../../../atoms/Flex';
import { Typo } from '../../../atoms/Typo';
import { Input } from '../../../atoms/Input';
import { memoize } from '../../../../helpers';
import { DropDownItem, dropDownMock } from './dropdownMock';
import { Icon } from '../../../atoms/Icon';
import { Spacer } from '../../../atoms/Spacer';

export const DropdownInput: React.FC = memoize(() => {
  const [currentValue, setCurrentValue] = useState(dropDownMock[0].label);

  const onAutocompleteSelect = useCallback((_: string, item: DropDownItem) => {
    setCurrentValue(item.label);
  }, []);

  const renderInput = useCallback(
    (props: React.HTMLAttributes<HTMLInputElement>) => {
      return (
        <Input
          placeholder="CERCA"
          style={{
            caretColor: 'transparent',
            cursor: 'pointer',
          }}
          {...props}
        />
      );
    },
    [],
  );

  const getItemValue = useCallback(
    (item: DropDownItem) => item.id.toString(),
    [],
  );

  const renderItem = useCallback((item: DropDownItem, isHilighted: boolean) => {
    return (
      <Flex.Box
        key={item.id}
        paddingBottom="m"
        paddingLeft="m"
        paddingRight="m"
        paddingTop="m"
        background={isHilighted ? 'light' : 'white'}
        alignItems="center"
      >
        <Icon iconName={item.icon} width="30px" />
        <Spacer width="35px" /> <Typo type="p">{item.label}</Typo>
      </Flex.Box>
    );
  }, []);

  return (
    <Autocomplete
      menuStyle={{
        borderRadius: '3px',
        boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
        background: 'rgba(255, 255, 255, 0.9)',
        padding: '2px 0',
        fontSize: '90%',
        position: 'fixed',
        overflow: 'auto',
        maxHeight: '50%',
        zIndex: 99,
      }}
      wrapperStyle={{ width: '25%' }}
      items={dropDownMock}
      value={currentValue}
      getItemValue={getItemValue}
      onSelect={onAutocompleteSelect}
      renderInput={renderInput}
      renderItem={renderItem}
    />
  );
});

DropdownInput.displayName = 'DropdownInput';