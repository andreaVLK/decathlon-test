import { locationApiBase } from '../utils';
import { LocationList } from '../types/general';

type Props = {
  fullTextToSearch: string;
};

export const searchLocationApi = async (
  { fullTextToSearch }: Props,
  controller: AbortController,
): Promise<LocationList> => {
  return locationApiBase
    .query({ fullTextToSearch })
    .signal(controller)
    .get()
    .json();
};
