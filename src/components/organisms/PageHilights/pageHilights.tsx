/**
 * @packageDocumentation
 * @module components/organisms
 */

import { useTheme } from 'styled-components';
import React, { useMemo } from 'react';
import { Col, Container, Row } from 'react-grid-system';
import { HilightCard } from '../../molecules/HilightCard';
import { pageHilightsMock } from './pageHilightsMock';
import { Theme } from '../../../styles';

export const PageHilights: React.FC = () => {
  const theme = useTheme() as Theme;

  const renderHilightList = useMemo(() => {
    return pageHilightsMock.map((mockItem) => (
      <Col key={mockItem.id} lg={4} xl={4}>
        <HilightCard
          icon={mockItem.icon}
          title={mockItem.title}
          text={mockItem.text}
        />
      </Col>
    ));
  }, []);
  return (
    <Container
      style={{ paddingTop: theme.spaces.l, paddingBottom: theme.spaces.xxl }}
    >
      <Row>{renderHilightList}</Row>
    </Container>
  );
};

PageHilights.displayName = 'PageHilights';
