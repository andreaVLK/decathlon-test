export type MatchedSubstring = {
  length: number;
  offset: number;
};

export type MainTextMatchedSubstring = {
  length: number;
  offset: number;
};

export type StructuredFormatting = {
  main_text: string;
  main_text_matched_substrings: MainTextMatchedSubstring[];
  secondary_text: string;
};

export type Term = {
  offset: number;
  value: string;
};

export type Place = {
  description: string;
  matched_substrings: MatchedSubstring[];
  place_id: string;
  reference: string;
  structured_formatting: StructuredFormatting;
  terms: Term[];
  types: string[];
};

export type PlacesApiResponse = {
  predictions: Place[];
};
