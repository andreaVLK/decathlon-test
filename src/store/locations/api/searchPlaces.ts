import { placesApiBase } from '../utils';
import { PlacesApiResponse } from '../types';

type Props = {
  input: string;
};

export const searchPlacesApi = async ({
  input,
}: Props, controller: AbortController): Promise<PlacesApiResponse> => {
  return placesApiBase
    .query({ input })
    .signal(controller)
    .get()
    .json();
};
