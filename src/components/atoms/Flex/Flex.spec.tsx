import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Flex } from './Flex';

describe('when render', () => {
  describe('box component', () => {
    it('should work with no props', () => {
      const tree = renderer.create(<Flex.Box />).toJSON();
      expect(tree).toHaveStyleRule('position', 'relative');
      expect(tree).toHaveStyleRule('display', 'flex');
      expect(tree).toHaveStyleRule('align-items', 'flex-start');
      expect(tree).toHaveStyleRule('justify-content', 'flex-start');
      expect(tree).toMatchSnapshot();
    });

    it('should work with flexBox Props', () => {
      const tree = renderer
        .create(<Flex.Box alignItems="center" justifyContent="space-around" />)
        .toJSON();
      expect(tree).toHaveStyleRule('position', 'relative');
      expect(tree).toHaveStyleRule('display', 'flex');
      expect(tree).toHaveStyleRule('align-items', 'center');
      expect(tree).toHaveStyleRule('justify-content', 'space-around');
      expect(tree).toMatchSnapshot();
    });
    it('should work with size Props', () => {
      const tree = renderer
        .create(
          <Flex.Box
            alignItems="center"
            justifyContent="space-around"
            width="100px"
            height="100px"
          />,
        )
        .toJSON();
      expect(tree).toHaveStyleRule('position', 'relative');
      expect(tree).toHaveStyleRule('display', 'flex');
      expect(tree).toHaveStyleRule('align-items', 'center');
      expect(tree).toHaveStyleRule('width', '100px');
      expect(tree).toHaveStyleRule('height', '100px');
      expect(tree).toHaveStyleRule('justify-content', 'space-around');
      expect(tree).toMatchSnapshot();
    });
  });
  describe('col component', () => {
    it('should work with no props', () => {
      const tree = renderer.create(<Flex.Col />).toJSON();
      expect(tree).toHaveStyleRule('position', 'relative');
      expect(tree).toHaveStyleRule('display', 'flex');
      expect(tree).toMatchSnapshot();
    });
    it('should work with size Props', () => {
      const tree = renderer
        .create(<Flex.Col width="100px" height="100px" />)
        .toJSON();
      expect(tree).toHaveStyleRule('position', 'relative');
      expect(tree).toHaveStyleRule('display', 'flex');
      expect(tree).toMatchSnapshot();
    });
  });
});
