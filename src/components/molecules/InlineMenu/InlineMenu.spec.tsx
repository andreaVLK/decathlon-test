import React from 'react';
import { ThemeProvider } from 'styled-components';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { configure, mount } from 'enzyme';
import { InlineMenu } from './InlineMenu';
import { menuMock } from './menuMock';
import { theme } from '../../../styles';

configure({ adapter: new Adapter() });

describe('when render', () => {
  describe('and has all props with a value', () => {
    const container = mount(
      <ThemeProvider theme={theme}>
        <InlineMenu
          items={menuMock}
        />,
      </ThemeProvider>,
    );

    it('should match snapshot', () => {
      expect(container.html()).toMatchSnapshot();
    });

    it('should contains n anchor as items length', () => {
      expect(container.find('a').length).toEqual(menuMock.length);
    });

    it('each item should contain an anchor and an image', () => {
      const anchor = container.find('a').first();
      const img = container.find('img').first();
      expect(anchor.get(0)).toBeDefined();
      expect(img.get(0)).toBeDefined();
    });

    it('label should be correct', () => {
      const anchorText = container.find('a').first().text();
      expect(anchorText).toEqual(menuMock[0].label);
    });
  });
});
