/**
 * @packageDocumentation
 * @module components/organisms
 */

import React from 'react';
import { Container, Row } from 'react-grid-system';
import logo from '../../../assets/images/logo.webp';
import { menuMock } from '../../molecules/InlineMenu/menuMock';
import { InlineMenu } from '../../molecules/InlineMenu';
import { Image } from '../../atoms/Image';
import { Flex } from '../../atoms/Flex';

export const PageHeader: React.FC = () => {
  return (
    <Container>
      <Row justify="between" align="center">
        <Flex.Box
          paddingTop="m"
          paddingBottom="m"
          width="100%"
          alignItems="center"
          justifyContent="space-between"
        >
          <Image src={logo} width="180px" />
          <InlineMenu items={menuMock} />
        </Flex.Box>
      </Row>
    </Container>
  );
};

PageHeader.displayName = 'PageHeader';
