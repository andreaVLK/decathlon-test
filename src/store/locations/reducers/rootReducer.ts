import { createReducer } from '@reduxjs/toolkit';
import { LocationState } from '../types/general';
import * as searchLocationCases from './search';
import * as resetLocationCases from './reset';
import * as setLocationQueryCases from './setQuery';
import { searchLocationActionTypes, resetLocationsActionTypes } from '../actions';
import { setLocationQueryActionTypes } from '../actions/setQuery';

const initialState: LocationState = {
  status: 'idle',
  data: [],
  error: null,
  query: '',
};

export const locationReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(
      searchLocationActionTypes.request,
      searchLocationCases.searchLocationRequestCase,
    )
    .addCase(
      searchLocationActionTypes.success,
      searchLocationCases.searchLocationSuccessCase,
    )
    .addCase(
      searchLocationActionTypes.failure,
      searchLocationCases.searchLocationErrorCase,
    )
    .addCase(
      resetLocationsActionTypes.reset,
      resetLocationCases.resetLocationsCase,
    )
    .addCase(
      setLocationQueryActionTypes.set,
      setLocationQueryCases.setLocationQueryCase,
    );
});
