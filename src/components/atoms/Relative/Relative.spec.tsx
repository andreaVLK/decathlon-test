import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Relative } from './Relative';

describe('when render', () => {
  it('should work with no props', () => {
    const tree = renderer.create(<Relative />).toJSON();
    expect(tree).toMatchSnapshot();
  });
  it('should work with size props', () => {
    const tree = renderer.create(<Relative width="100px" height="100px" />).toJSON();
    expect(tree).toHaveStyleRule('width', '100px');
    expect(tree).toHaveStyleRule('height', '100px');
    expect(tree).toMatchSnapshot();
  });

  it('should work with position props', () => {
    const tree = renderer.create(<Relative width="100px" height="100px" top="30px" left="100px" />).toJSON();
    expect(tree).toHaveStyleRule('width', '100px');
    expect(tree).toHaveStyleRule('height', '100px');
    expect(tree).toHaveStyleRule('top', '30px');
    expect(tree).toHaveStyleRule('left', '100px');
    expect(tree).toMatchSnapshot();
  });
});

