/**
 * @packageDocumentation
 * @module components/atoms
 */

import styled from 'styled-components';
import { Theme } from '../../../styles/types';
import { memoize } from '../../../helpers';

export type Props = {
  width?: string;
  theme: Theme;
};

export const Input = memoize(styled.input<Props>`
  position: relative;
  width: ${({ width = '100%' }: Props) => width};
  min-height: 45px;
  padding-top: ${({ theme }: Props) => theme?.spaces?.xxs || '5px'};
  padding-bottom: ${({ theme }: Props) => theme?.spaces?.xxs || '5px'};
  padding-left: ${({ theme }: Props) => theme?.spaces?.m || '20px' };
  padding-right: ${({ theme }: Props) => theme?.spaces?.m || '20px'};
  font-size: ${({ theme }: Props) => theme?.fontSizes?.m || '16px'};
  font-family: ${({ theme }: Props) => theme?.fontFamilies?.serif || 'arial'};
  text-transform: uppercase;
  outline: none;
  border: 1px solid ${({ theme }: Props) => theme?.colors?.dark || '#999'};
  box-sizing: border-box;
  background: ${({ theme }: Props) => theme?.colors?.white || '#fff'};
`);

Input.displayName = 'Input';
