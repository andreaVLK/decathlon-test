import { SearchLocationSuccessAction } from '../types';
import { LocationState } from '../types/general';
import { SearchLocationErrorAction } from '../types/search';

export const searchLocationRequestCase = (
  state: LocationState,
): LocationState => ({
  ...state,
  status: 'loading',
});

export const searchLocationSuccessCase = (
  state: LocationState,
  action: SearchLocationSuccessAction,
): LocationState => ({
  ...state,
  status: 'loaded',
  data: action.payload,
});

export const searchLocationErrorCase = (
  state: LocationState,
  action: SearchLocationErrorAction,
): LocationState => ({
  ...state,
  status: 'idle',
  error: action.payload,
});
