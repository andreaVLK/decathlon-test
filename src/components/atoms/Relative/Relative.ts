/**
 * @packageDocumentation
 * @module components/atoms
 */

import styled from 'styled-components';

type Props = {
  top?: string;
  left?: string;
  width?: string;
  height?: string;
  zIndex?: number;
};

export const Relative = styled.div<Props>`
  position: relative;
  top: ${({ top = '0' }) => top };
  left: ${({ left = '0' }) => left };
  width: ${({ width = 'auto' }) => width };
  height: ${({ height = 'auto' }) => height };
  z-index: ${({ zIndex = 'auto' }) => zIndex };
`;

Relative.displayName = 'Relative';
