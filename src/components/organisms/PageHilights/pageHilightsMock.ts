import { IconName } from '../../atoms/Icon/IconMap';

type PageHilightItem = {
  id: number;
  icon: IconName;
  title: string;
  text: string;
};

export const pageHilightsMock: PageHilightItem[] = [
  {
    id: 1,
    icon: 'multisport',
    title: 'MULTI-SPORT',
    text: 'Non solo sci, nel nostro catalogo puoi noleggiare attrezzatura per praticare tanti sport (sci, snowboard, sci freeride, sci alpinismo, sci di fondo, slittini, bici, SUP, Kayak, monopattini, tennis).',
  },
  {
    id: 2,
    icon: 'capillarity',
    title: 'CAPILLARITÁ',
    text: 'Presso i nostri negozi Decathlon, vicino a te in città o dai nostri PARTNER, nelle migliori località turistiche d\'Italia sia al mare che in montagna.',
  },
  {
    id: 3,
    icon: 'money',
    title: 'MIGLIOR PREZZO',
    text: 'Nei nostri negozi puoi ritirare il prodotto il giorno prima del tuo noleggio dalle 17 e consegnarlo il giorno dopo entro le 14, al miglior prezzo. Dai PARTNER sconti fino al -50% per chi prenota online.',
  },
];
