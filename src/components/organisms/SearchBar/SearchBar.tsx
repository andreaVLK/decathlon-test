/**
 * @packageDocumentation
 * @module components/organisms
 */

import React from 'react';
import { Col, Container, Row } from 'react-grid-system';
import { Button } from '../../atoms/Button';
import { Relative } from '../../atoms/Relative';
import { DropdownInput } from './DropdownInput';
import { AutocompleteInput } from './AutocompleteInput';
import { Wrapper } from './Wrapper';
import { CalendarInput } from './CalendarInput/CalendarInput';

export const SearchBar: React.FC = () => {
  return (
    <Container>
      <Row>
        <Col>
          <Relative top="-40px">
            <Wrapper>
              <AutocompleteInput />
              <DropdownInput />
              <CalendarInput />
              <Button type="button" width="15%">
                CERCA
              </Button>
            </Wrapper>
          </Relative>
        </Col>
      </Row>
    </Container>
  );
};

SearchBar.displayName = 'SearchBar';
