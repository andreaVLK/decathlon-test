class SagaAbortController {
  controller: AbortController;

  constructor() {
    this.controller = new AbortController();
  }

  getControllers() {
    const prevController = this.controller;
    const newController = new AbortController();
    this.controller = newController;
    return {
      prevController,
      controller: newController,
    };
  }
}

export const sagaAbortController = new SagaAbortController();