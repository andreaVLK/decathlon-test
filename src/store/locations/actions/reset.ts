import { createAction } from '@reduxjs/toolkit';

export const resetLocationsActionTypes = {
  reset: 'action/RESET_LOCATIONS',
};

export const resetLocations = createAction(resetLocationsActionTypes.reset);
