# Decathlon test  - Andrea Simone Porceddu

:warning: **YOU MUST RUN BOTH TEST AND APP TO WORK ON**
:warning: **KEEP TEST UPDATED**
:warning: **BE SURE YOUR IDE IS CORRECTLY CONFIGURED FOR ESLINT AND PRETTIER**


---
## Scripts

### Run application

    $ npm start

### Run with location api mock

    $ npm run start-mock

### Build application 

    $ npm run isReady

### Eject application

:warning: **WARNING!! YOU CAN'T DISCARD THIS**

    $ npm run eject

### Clean application 
clean `node_modules package-lock test-snapshot docs` 

    $ npm run clean

### Run test

    $ npm run test

### Lint code

    $ npm run lint

### format code

    $ npm run format

### generate docs

    $ npm run doc

---

## Main Stack

- **react** (main)
- **typescript** (language)
- **@redux/toolkit** (state management)
- **redux-saga** (reactive async state management)
- **react-redux** (react redux adapter)
- **styled-components** (theming, components, styling)
- **date-fns** (time computation)
- **jest** (test)
- **jest** (enzyme)
- **jest-styled-comoponent** (test on styled components)

---

## File system

The filesystem is scope based. 
:warning: **Do not place files on inappropriate folder**

```
src 
│
└─ components
|    |
│    └─ atoms
|    └─ molecules    
|    └─ organism
│   
└─ helpers 
|
└─ pages
|
└─ store 
|    |
│    └─ {functionalDomain}
|          |
|          └─ actions    
|          └─ api
|          └─ constants
|          └─ reducers
|          └─ sagas
|          └─ types
|          └─ utils
|
└─ styles
|
└─ types
|
| App.tsx
| AppProvided.tsx
| ConfigureStore.ts
| index.tsx

```

---

## Test

This Repo si actually mainly covered by unit test.
To run test and get the updated coverage: `$ npm run test`

### Coverage

| Statements | 63.13% ( 113/179 ) |
|------------|--------------------|
| Branches   | 84.5% ( 109/129 )  |
| Functions  | 70.48% ( 74/105 )  |
| Lines      | 64.37% ( 112/174 ) |

---

## Atomic desing pattern

# Atomic desing Pattern in React

Atomic desing pattern is a desing concept that can be moved to React apps development and help developers to better organize and split ui components and idealize their roles.

We have defined some rules to help you to identify your components and place it in this schema.

## 1.Folder structure

```

components
|
|--atoms
| |
| |--Button
| | |
| | |--Button.tsx
| | |--index.ts
| |
| |--Icon
| | |
| | |--Icon.tsx
| | |--index.ts
| |
| |--Image
| | |
| | |--Icon.tsx
| | |--index.ts
| |
| |--index.ts
|
|--molecules
| |
| |--IconButton
| | |
| | |--IconButton.tsx
| | |--index.ts
| |
| |--index.ts
|
|--organisms
| |
| |--ImageLikeForm
| | |
| | |--{partialComponentFolder}
| | | |
| | | |-ImageLikeFormHeader.tsx
| | | |-ImageLikeFormBottom.tsx
| | | |-index.ts
| | |
| | |--ImageLikeForm.tsx
| | |--index.ts
| |
| |--index.ts
|
|--templates
| |
| |--TwoColumnPage
| | |
| | |--{partialComponentFolder}
| | | |
| | | |--TwoColumnPageMainColumn.tsx
| | | |--TwoColumnPageSideColumn.tsx
| | |  
| | |--TwoColumnPage.tsx
| | |--Index.ts
| |
| |-index.ts
|
|--index.ts

```
---

## 2. Atoms Components

Atoms are the smallest particles of our ui microsistem.

### 2.1 Rules for Atoms

- Atoms does not import others components.
- Atoms usually render one HTML element.
- Atoms know nothing about application state.
- Atoms have not partials.

### 2.2 Samples of Atoms

#### 2.2.1 With React functional components
```javascript
const styles = { background: 'green' }
const Button = (children) => {
  render (
    <button style={styles} type="button">
      {children}
    </button>
  )
}
```
#### 2.2.2 With Styled Components
```javascript
const StyledButton = styled`
  button: green;
`
```

---

## 3. Molecules Components

Molecules are compositions of two or more atoms, to make more ready to use components for your interface.

### 3.1 Rules for Molecules

- Molecules import two or more Atoms.
- Molecules do not contain HTML tags
- Molecules know nothing about application state.
- Molecules have not partials.

### 3.2 Samples of Molecules
```javascript
import { Button, Icon } from '../atoms'
const IconButton = (children) => {
  render (
    <Button>
      <Icon name="like"/><{children} />
    </Button>
  )
}
```
---

## 4. Organisms Components

Organisms combine two or more molecules(some times atoms) in order to create more complex components that could be connected with your application state.
Organism could be optionally splitted in folder based domain of data.

### 4.1 Rules for Organisms

- Organisms imports two ore more molecules
- Organism do not contain HTML tags
- Organisms could be connect with application state.
- Organism could have partials (sample: _listItem_)
- Organism could be optionally splitted in folder based domain of data.

### 4.2 Samples of Organisms
 
```javascript

    import {
      InfoCard,
      Grid,
      Col,
    } from '../molecules'

    const PersonsGridList = ({ persons }) => {

      const renderPersonsGrid = () => (
        persons.map(person => (
          <Col lg={3}>
            <InfoCard photo={person.photo} name={person.name} />
          </Col>
        ))
      )

      render (
        <Grid>
          {renderPersonsGrid}
        </Grid>
      )
    }
```
---

## 5. Templates Components Systems

Templates are system of components that provide layout for you pages.

### 5.1 Rules for Templates

- Templates are skeletons
- Templates are usually composed by container and children components
- Templates have no contents
- Templates never know about component state

### 5.2 Samples of a Template folder
```
templates
|
|--TwoColumnPage
| |
| |--{partialComponent}
| | |
| | |--TwoColumnPageMainColumn.tsx
| | |--TwoColumnPageSideColumn.tsx
| |  
| |--TwoColumnPage.tsx
| |--Index.ts
|
|--index.ts
```
## 6. Pages (or Routes/Screen)

Pages are simply our route components. The use templates to Wrap Organism.
These components are placed outside the components folder, inside the routes components.

### 6.1 Sample of a page
```javascript
import { TwoColumnPage } from '../components/templates'
import { Menu, ProfileResume } from '../components/templates'
const page = () => (
  <TwoCoumnPage>
    <TwoColumnPageSideColumn>
      <Menu />
    </TwoColumnPageSideColumn>
    <TwoColumnPageMainColumn>
      <ProfileResume />
    </TwoColumnPageMainColumn>
  </TwoCoumnPage>
)
```
---

# Theming

All components are provided by a typed theme.

:warning: **Theme should not be changed. Please contact the design team if you found some issue on actual configuration.**

```javascript
<Theme theme={theme}>
  <App />
</Theme>
```

---

## Redux and state management

### State develpment in detail

### Core libraries

#### State management

- redux
- redux-saga
- reselect
- typesafe-actions

#### Time computation

- dayjs

#### Testing

- jest

---

### File system

```ts
src
|
|--constants
|  |
|  |--{scope}Constants.ts
|  |--{scope2}Constants.ts
|  |--general.ts
|  |--index.ts
|
|--utils
|  |
|  |
|  |--{scope}Utils.ts
|  |--index.ts
|
|--{dataDomainName}
|  |
|  |--actions
|  |  |
|  |  |--{actionName}.ts
|  |  |--{actionName2}.ts
|  |  |--index.ts
|  |
|  |--api
|  |  |
|  |  |--{actionName}.ts
|  |  |--{actionName2}.ts
|  |  |--index.ts
|  |
|  |--reducers
|  |  |
|  |  |--{actionName}.ts
|  |  |--{actionName2}.ts
|  |  |--rootReducer.ts
|  |  |--index.ts
|  |
|  |--sagas
|  |  |
|  |  |--{actionName}.ts
|  |  |--{actionName2}.ts
|  |  |--rootSaga.ts
|  |  |--index.ts
|  |
|  |
|  |--selectors
|  |  |
|  |  |--{scope}.ts
|  |  |--{scope2}.ts
|  |  |--index.ts
|  |
|  |--types
|  |  |
|  |  |--{actionName}.ts
|  |  |--{actionName2}.ts
|  |  |--general.ts
|  |  |--index.ts
|  |
|  |--utils
|  |  |
|  |  |--{scope1}.ts
|  |  |--{scope2}.ts
|  |  |--index.ts
|  |
|  |--index.ts
|
|--index.ts
|--rootReducers.ts
|--rootSagas.ts
|--types.ts
```

---

### Tips

- Design states that are easy to use.
- Convert Array lists to objects to keep data more accessible
- Avoid the creation of actions that need too many params, or better use `yield select(selectorName)` to take params directly from state inside sagas. In this sample the filter has been setted previously with another action and after selected with the saga `select` helper. In this way you don't need to pass any parameters to the action.

```javascript
export function* fetchSpendingBudgetsSaga(
  extraArguments: ExtraArguments
): SagaIterator {
  yield put(fetchSpendingBudgetsRequest());
  const filter = yield select(getSpendingFilter);
  const { c6consent } = filter;
  try {
    const { status, payload, errors }: BudgetListApiResponse = yield call(
      fetchSpendingBudgetsApi,
      c6consent,
      extraArguments.BASE_URL
    );
    if (status === SUCCESS_STATUS) {
      yield put(fetchSpendingBudgetsSuccess(payload));
    } else {
      yield put(fetchSpendingBudgetsFailure({ errors }));
    }
  } catch (e) {
    yield put(fetchSpendingBudgetsFailure({ errors: [e] }));
  }
}
```

- Move computations and logic into the state. Sagas are a good place to move logic and manage series of async events, bringing them in a sync sequence. It keep components clean e logicless.
- Use just selector to bring data to UI components. Use them to get computed parts of state, or simply get slices of it. It keep component cleen and logicless and increase the render performance.

---

### Data flow

```
                               > APIS ---
                              |            |
                              |            v
 > UI > ACTIONS > SAGAS(MIDDLEWARE) > EFFECTS---
|                                               |
|                                               |
 --- SELECTORS < STORE < CASES < REDUCERS <-----
```

---

### State Model(sync)

All states linked to sync actions and domains should contain a plain object of data

```javascript
type sampleState = {
  startDate: string,
  ensDate: string,
};
```

---

### State Model(async)

All states should have this structure

```javascript
type sampleState = {
  data: {},
  status: string,
  error?: object,
};
```

---

#### data

`data` contains the main data of each state

#### isFetching, isFetched

`status` provide the status for async state

#### errors

`error` provide **errors** when an action got errors

**\*Note**: We would like to move **errors** and **fetching** management to a centralized system.\*

#### Optimistic data flow

Some state can be managed with an optimistic data flow. Use this pattern is not a constraint and should be applied with caution after a exaustive team evaluation.
