import { PayloadAction } from '@reduxjs/toolkit';
import { LocationList, StateError } from './general';

export type SearchLocationSuccessPayload = LocationList;

export type SearchLocationErrorPayload = StateError;

export type SearchLocationSuccessAction = PayloadAction<
SearchLocationSuccessPayload
>;

export type SearchLocationErrorAction = PayloadAction<StateError>;
