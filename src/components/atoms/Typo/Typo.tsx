/**
 * @packageDocumentation
 * @module components/atoms
 */

import React, { useMemo } from 'react';
import styled, { css } from 'styled-components';
import { map, Type, DefaultStyle } from './styleMap';
import { ThemeColor, ThemeSize, Theme } from '../../../styles/types';
import { memoize } from '../../../helpers';

type Props = {
  type: Type;
  color?: ThemeColor;
  size?: ThemeSize;
};

type StyledTypo = {
  theme: Theme;
  defaultStyle: DefaultStyle;
} & Pick<Props, 'color' | 'size'>;

const defaultTypo = css<StyledTypo>`
  font-family: ${({ theme }: StyledTypo) =>
    theme?.fontFamilies && theme?.fontFamilies?.serif || 'Arial'};

  font-size: ${({ theme, defaultStyle, size }: StyledTypo) =>
    size ? theme?.fontSizes[size] : theme.fontSizes && theme?.fontSizes[defaultStyle.size] || '18px'};

  font-weight: ${({ theme, defaultStyle }: StyledTypo) =>
    theme?.fontWeights && theme?.fontWeights[defaultStyle.weight] || '400'};

  color: ${({ theme, defaultStyle, color }: StyledTypo) =>
    color ? theme?.colors[color] : theme.colors && theme?.colors[defaultStyle.color] || '#000'};

  font-style: ${({ theme, defaultStyle }: StyledTypo) =>
    theme?.fontStyles && theme?.fontStyles[defaultStyle.style] || 'normal'};

  ${(({ defaultStyle }: StyledTypo) =>
    defaultStyle.shadow === 'large' &&
    css`
      text-shadow: 2px 2px 4px rgba(0, 0, 0.1);
    `)};
  ${(({ defaultStyle }: StyledTypo) =>
    defaultStyle.shadow === 'small' &&
    css`
      text-shadow: 1px 1px 2px rgba(0, 0, 0.1);
    `)};
  ${(({ defaultStyle }: StyledTypo) =>
    defaultStyle.cursor === 'pointer' &&
    css`
      cursor: pointer;
    `)};
  ${(({ defaultStyle }: StyledTypo) =>
    defaultStyle.withHoverFx &&
    css`
      &:hover {
        opacity: 0.7;
      }
    `)};
`;

const H1 = memoize(styled.h1<StyledTypo>`
  ${defaultTypo}
`);

const H2 = memoize(styled.h2<StyledTypo>`
  ${defaultTypo}
`);

const H3 = memoize(styled.h3<StyledTypo>`
  ${defaultTypo}
`);

const P = memoize(styled.p<StyledTypo>`
  ${defaultTypo}
`);

const A = memoize(styled.a<StyledTypo>`
  ${defaultTypo}
`);

const styledTypoTypes: {
  [key: string]: React.FC<Omit<StyledTypo, 'theme'>>;
} = {
  h1: H1,
  h2: H2,
  h3: H3,
  p: P,
  ps: P,
  pxs: P,
  a: A,
};

export const Typo: React.FC<Props> = memoize(
  ({ type, color, size, children }) => {
    const defaultStyle: DefaultStyle = map[type];

    const StyledTypography = useMemo(() => styledTypoTypes[type], [type]);

    return (
      <StyledTypography defaultStyle={defaultStyle} color={color} size={size}>
        {children}
      </StyledTypography>
    );
  },
);

Typo.displayName = 'Typo';
