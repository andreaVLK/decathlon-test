import { locationMock } from '../locationMock';

export const getLocationMock = () => {
  const randomicNumber = Math.random();
  return randomicNumber > 0.5 ? [] : locationMock;
};