import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Image } from './Image';

describe('when render', () => {
  it('should work with no props', () => {
    const tree = renderer.create(<Image />).toJSON();
    expect(tree).toHaveStyleRule('width', '100%');
    expect(tree).toHaveStyleRule('height', 'auto');
    expect(tree).toHaveStyleRule('object-fit', 'contain');
    expect(tree).toMatchSnapshot();
  });

  it('should work with size props', () => {
    const tree = renderer.create(<Image width="100px" height="100px" />).toJSON();
    expect(tree).toHaveStyleRule('width', '100px');
    expect(tree).toHaveStyleRule('height', '100px');
    expect(tree).toHaveStyleRule('object-fit', 'contain');
    expect(tree).toMatchSnapshot();
  });

  it('should work with object fit props', () => {
    const tree = renderer.create(<Image objectFit="cover" />).toJSON();
    expect(tree).toHaveStyleRule('width', '100%');
    expect(tree).toHaveStyleRule('height', 'auto');
    expect(tree).toHaveStyleRule('object-fit', 'cover');
    expect(tree).toMatchSnapshot();
  });
});
