import wretch from 'wretch';

export const placesApiBase = wretch(
  process.env.REACT_APP_GOOGLE_PLACES_BASE_URL,
).query({
  key: process.env.REACT_APP_GOOGLE_API_KEY,
  components: 'country:it',
});
