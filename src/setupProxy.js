const { createProxyMiddleware } = require('http-proxy-middleware');

// eslint-disable-next-line func-names
module.exports = function (app) {
  app.use(
    '/google-place-api',
    createProxyMiddleware({
      target: 'https://maps.googleapis.com/maps/api/place/autocomplete/json',
      changeOrigin: true,
      pathRewrite: { '/google-place-api': '' },
    }),
  );
};
