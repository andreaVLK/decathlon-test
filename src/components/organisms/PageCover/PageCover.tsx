/**
 * @packageDocumentation
 * @module components/organisms
 */

import React from 'react';
import { Cover } from '../../molecules/Cover';
import snowboard from '../../../assets/images/photos/snowboard.jpg';
import { memoize } from '../../../helpers';

export const PageCover: React.FC = memoize(() => {
  return (
    <Cover
      image={snowboard}
      bigMessage={<>NOLEGGIA SCI E<br />SNOWBOARD<br />ONLINE</>}
      smallMessage="NEI NOSTRI NEGOZI E NEI NOSTRI STORE ONLINE"
    />
  );
});

PageCover.displayName = 'PageCover';


