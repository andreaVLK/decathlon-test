import { IconName } from '../../../atoms/Icon/IconMap';

export type DropDownItem = {
  id: number;
  icon: IconName;
  label: string;
};

export const dropDownMock: DropDownItem[] = [
  {
    id: 1,
    icon: 'snowboard',
    label: 'SNOWBOARD',
  },
  {
    id: 2,
    icon: 'bike',
    label: 'BICICLETTA',
  },
  {
    id: 3,
    icon: 'ski',
    label: 'SCI',
  },
  {
    id: 4,
    icon: 'xcski',
    label: 'SCI DI FONDO',
  },
  {
    id: 5,
    icon: 'skitouring',
    label: 'SCI TURISMO',
  },
  {
    id: 6,
    icon: 'kayak',
    label: 'KAYAK',
  },
  {
    id: 7,
    icon: 'sledding',
    label: 'SLITTINI',
  },
];
