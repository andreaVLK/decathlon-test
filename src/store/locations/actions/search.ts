import { createAction } from '@reduxjs/toolkit';
import { SearchLocationErrorPayload } from '../types';
import { SearchLocationSuccessPayload } from '../types/search';

export const searchLocationActionTypes = {
  trigger: 'trigger/SEARCH_LOCATION',
  request: 'action/SEARCH_LOCTION_REQUEST',
  success: 'action/SEARCH_LOCATION_SUCCESS',
  failure: 'action/SEARCH_LOCATION_FAILURE',
  debounce: 'support/SEARCH_LOCATION_DEBOUNCE',
  canceled: 'support/SEARCH_LOCATION_CANCELED',
};

export const searchLocation = createAction(
  searchLocationActionTypes.trigger,
);

export const searchLocationRequest = createAction(
  searchLocationActionTypes.request,
);

export const searchLocationSuccess = createAction<SearchLocationSuccessPayload>(
  searchLocationActionTypes.success,
);

export const searchLocationFailure = createAction<SearchLocationErrorPayload>(
  searchLocationActionTypes.failure,
);
