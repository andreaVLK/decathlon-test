import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Spacer } from './Spacer';

describe('when render', () => {
  it('should work with no props', () => {
    const tree = renderer.create(<Spacer />).toJSON();
    expect(tree).toMatchSnapshot();
  });
  it('should work with size props', () => {
    const tree = renderer.create(<Spacer width="100px" height="100px" />).toJSON();
    expect(tree).toHaveStyleRule('width', '100px');
    expect(tree).toHaveStyleRule('height', '100px');
    expect(tree).toMatchSnapshot();
  });
});

