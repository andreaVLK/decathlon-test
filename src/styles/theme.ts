import { Theme } from './types';

const colors = {
  primary: '#2a98dd',
  secondary: '#feed00',
  black: '#000000',
  white: '#fff',
  dark: '#999',
  accent: 'orange',
  light: '#eee',
};

const spaces = {
  xxs: '3px',
  xs: '10px',
  s: '15px',
  m: '20px',
  l: '30px',
  xl: '50px',
  xxl: '70px',
  defaultPagePadding: '40px',
};

const fontSizes = {
  xxs: '9px',
  xs: '12px',
  s: '14px',
  m: '16px',
  l: '18px',
  xl: '30px',
  xxl: '80px',
};

const fontStyles = {
  default: 'normal',
  italic: 'italic',
};

const fontWeights = {
  regular: '400',
  bold: '700',
};

const fontFamilies = {
  serif: '\'Roboto Condensed\', sans-serif',
};

export const theme: Theme = {
  colors,
  spaces,
  fontSizes,
  fontFamilies,
  fontStyles,
  fontWeights,
};
