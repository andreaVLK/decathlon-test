/**
 * @packageDocumentation
 * @module components/atoms
 */

import styled from 'styled-components';
import { memoize } from '../../../helpers';

type Props = {
  top?: string;
  left?: string;
  width?: string;
  height?: string;
  zIndex?: number;
};

export const Absolute = memoize(styled.div<Props>`
  position: absolute;
  top: ${({ top = '0' }) => top };
  left: ${({ left = '0' }) => left };
  width: ${({ width = 'auto' }) => width };
  height: ${({ height = 'auto' }) => height };
  z-index: ${({ zIndex = 'auto' }) => zIndex };
`);

Absolute.displayName = 'Absolute';
