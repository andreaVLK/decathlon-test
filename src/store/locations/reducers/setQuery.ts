import { LocationState } from '../types/general';
import { SetLocationQueryAction } from '../types/setQuery';

export const setLocationQueryCase = (
  state: LocationState,
  action: SetLocationQueryAction,
): LocationState => ({
  ...state,
  query: action.payload.query,
});
