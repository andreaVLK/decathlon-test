import { combineReducers, configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import { locationReducer } from './store/locations/reducers/rootReducer';
import { searchLocationRootSaga } from './store/locations/sagas/rootSaga';

const sagaMiddleware = createSagaMiddleware();

const reducer = combineReducers({
  location: locationReducer,
});

const sagas = [searchLocationRootSaga];

export const store = configureStore({
  reducer,
  middleware: [sagaMiddleware],
  devTools: process.env.NODE_ENV !== 'production',
});

sagas.forEach((saga) => {
  sagaMiddleware.run(saga);
});






