import React from 'react';
import { PageHeader } from '../../components/organisms/PageHeader';
import { PageCover } from '../../components/organisms/PageCover';
import { SearchBar } from '../../components/organisms/SearchBar';
import { PageHilights } from '../../components/organisms/PageHilights';

export const Home = () => {
  return (
    <>
      <PageHeader />
      <PageCover />
      <SearchBar />
      <PageHilights />
    </>
  );
};
