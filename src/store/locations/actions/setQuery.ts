import { createAction } from '@reduxjs/toolkit';
import { SetLocationQueryPayload } from '../types/setQuery';

export const setLocationQueryActionTypes = {
  set: 'action/SET_LOCATION_QUERY',
};

export const setLocationQuery = createAction<SetLocationQueryPayload>(
  setLocationQueryActionTypes.set,
);
