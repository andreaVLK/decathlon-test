import { StateError } from '../types';

type ErrorName = 'FAILED' | 'ABORTED';

type DefaultError = Record<ErrorName, StateError>;

export const DEFAULT_ERRORS: DefaultError = {
  FAILED: {
    code: 1,
    message: 'Search is failed',
  },
  ABORTED: {
    code: 1,
    message: 'Search is aborted',
  },
};