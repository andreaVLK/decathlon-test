import { call, put, select } from 'redux-saga/effects';
import { Status } from '../types';
import {
  searchLocationFailure,
  searchLocationRequest,
  searchLocationSuccess,
} from '../actions';
import { searchLocationApi } from '../api/search';
import { getLocationQuery, getLocationStatus } from '../selectors';
import { getLocationMock, sagaAbortController, convertPlacesToLocations } from '../utils';
import { DEFAULT_ERRORS } from '../constants/errors';
import { searchPlacesApi } from '../api/searchPlaces';
import { LocationList } from '../types/general';

export function* searchLocationSaga() {
  const { prevController, controller } = sagaAbortController.getControllers();
  const query: string = yield select(getLocationQuery);
  const status: Status = yield select(getLocationStatus);
  if (!query) {
    return null;
  }
  yield put(searchLocationRequest());
  try {
    if (status === 'loading') {
      prevController.abort();
    }

    const response = process.env.REACT_APP_USE_LOCATION_MOCK === 'ACTIVE' ? 
      getLocationMock() : 
      yield call(
        searchLocationApi,
        { fullTextToSearch: query },
        controller,
      );

    if (!response?.length) {
      const placesResponse = yield call(
        searchPlacesApi,
        { input: query },
        controller,
      );

      const placesConverted: LocationList = convertPlacesToLocations(
        placesResponse,
      );

      return yield put(searchLocationSuccess(placesConverted));
    }
    return yield put(searchLocationSuccess(response));
  } catch (err) {
    if (controller.signal.aborted) {
      return yield put(searchLocationFailure(DEFAULT_ERRORS.ABORTED));
    }
    return yield put(searchLocationFailure(DEFAULT_ERRORS.FAILED));
  }
}
