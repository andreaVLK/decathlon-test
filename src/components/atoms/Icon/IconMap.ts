// flags
import en from '../../../assets/images/flag-icons/en.webp';
import it from '../../../assets/images/flag-icons/it.webp';

// hilight
import blueMoney from '../../../assets/images/hilight-icons/blue_money.svg';
import capillarity from '../../../assets/images/hilight-icons/capillarity.svg';
import multisport from '../../../assets/images/hilight-icons/multisport.svg';

// menu
import cart from '../../../assets/images/menu-icons/cart.svg';
import faq from '../../../assets/images/menu-icons/faq.svg';
import mail from '../../../assets/images/menu-icons/mail.svg';
import money from '../../../assets/images/menu-icons/money.svg';
import partner from '../../../assets/images/menu-icons/partner.svg';
import shop from '../../../assets/images/menu-icons/shop.svg';
import user from '../../../assets/images/menu-icons/user.svg';

// sports
import bike from '../../../assets/images/sport-icons/bike.svg';
import kayak from '../../../assets/images/sport-icons/kayak.svg';
import scooter from '../../../assets/images/sport-icons/scooter.svg';
import ski from '../../../assets/images/sport-icons/ski.svg';
import skitouring from '../../../assets/images/sport-icons/skitouring.svg';
import sledding from '../../../assets/images/sport-icons/sledding.svg';
import snowboard from '../../../assets/images/sport-icons/snowboard.svg';
import snowshoes from '../../../assets/images/sport-icons/snowshoes.svg';
import tennis from '../../../assets/images/sport-icons/tennis.svg';
import xcski from '../../../assets/images/sport-icons/xcski.svg';



export const icons = {
  // flags
  en,
  it,
  // hilights
  blueMoney,
  capillarity,
  multisport,
  // menu
  cart,
  faq,
  mail,
  money,
  partner,
  shop,
  user,
  // sports
  bike,
  kayak,
  scooter,
  ski,
  skitouring,
  sledding,
  snowboard,
  snowshoes,
  tennis,
  xcski,
};

export type IconMap = typeof icons;

export type IconName = keyof typeof icons;
