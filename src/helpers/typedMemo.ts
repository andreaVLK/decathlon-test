import React from 'react';

export const memoize: <T>(c: T) => T = React.memo;
