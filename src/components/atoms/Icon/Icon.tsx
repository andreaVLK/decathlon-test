/**
 * @packageDocumentation
 * @module components/atoms
 */

import React from 'react';
import { Image } from '../Image';
import { IconName, icons } from './IconMap';
import { memoize } from '../../../helpers';

type Props = {
  width: string;
  height?: string;
  iconName: IconName;
};

export const Icon: React.FC<Props> = memoize(({ width, height, iconName }) => {
  const src = icons[iconName];

  if (src?.indexOf('.svg') > -1) {
    return <img width={width} height={height} src={src} alt='svg' />;
  }

  return src ? <Image width={width} height={height} src={src} /> : null;
});

Icon.displayName = 'Icon';
