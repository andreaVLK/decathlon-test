/**
 * @packageDocumentation
 * @module components/atoms
 */

import styled from 'styled-components';

type Props = {
  width?: string;
  height?: string;
  objectFit?: string;
};

export const Image = styled.img<Props>`
  width: ${({ width = '100%' }) => width};
  height: ${({ height = 'auto' }) => height};
  object-fit: ${({ objectFit = 'contain' }) => objectFit};
`;


Image.displayName = 'Image';